package pages;

import io.qameta.allure.Step;
import org.asynchttpclient.util.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class PochtaMainPage {
    By loginButton = new By.ByXPath("//div[@class='Box-sc-7ax6ia-0 cquGUr']");
    By inputLogin = new By.ById("username");
    By inputPassword = new By.ById("userpassword");
    By login = new By.ByXPath("//span[text()='Войти']");

    By departures = new By.ByXPath("//a[text()='Мои отправления']");
    By text = new By.ByXPath("//p[@class='Paragraph-sc-10hckd4-0 fhoCWJ']");

    public PochtaMainPage(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver driver;

    @Step("Открытие портальной страницы")
    public PochtaMainPage open() {
        driver.get("https://www.pochta.ru/");
        return this;
    }

    @Step("нажатие кнопки Войти портал")
    public void clickLogin() {
        driver.findElement(loginButton).click();
    }

    @Step("авторизация на портале")
    public void autorizationParameters() {
        driver.findElement(inputLogin).sendKeys("tojoxi2718@mahazai.com");
        driver.findElement(inputPassword).sendKeys("1QAZ2wsx");
        driver.findElement(login).click();
    }

    @Step("переход в Мои отправления")
    public void departuresOpen() {
        WebElement element = driver.findElement(By.xpath("//div[@class='Box-sc-7ax6ia-0 Headerstyles__UserNameContainer-sc-1rbx3hp-9 iztNsN gMVagu']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
        driver.findElement(departures).click();

    }
    @Step("текст")
    public void checkText() {
        driver.findElement(text).getText();

    }
}

