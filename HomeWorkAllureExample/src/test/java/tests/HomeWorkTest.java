package tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.PochtaMainPage;

@DisplayName("Тесты яндекса 2")
public class HomeWorkTest extends BaseTest {

  private PochtaMainPage pochtaMainPage;

  @BeforeEach
  public void initPage(){
    pochtaMainPage = new PochtaMainPage(driver);
  }

  @Test
  @Feature("Проверка текста")
  @DisplayName("Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся")
  @Severity(SeverityLevel.NORMAL)
  public void firstTest(){
    pochtaMainPage.open()
            .clickLogin();
    pochtaMainPage.autorizationParameters();
    pochtaMainPage.open();
    pochtaMainPage.departuresOpen();
    pochtaMainPage.checkText();

    Assertions.assertEquals("Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся", driver.getTitle());
  }

}
